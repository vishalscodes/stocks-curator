# Stocks Curator

Watch the Indian stock market without the advertisements and tracking.

# Disclaimer

Stocks Curator does not host any content. All content on Stocks Curator is sourced from MoneyControl (specifically [https://www.moneycontrol.com/stocksmarketsindia](https://www.moneycontrol.com/stocksmarketsindia)). Stocks Curator is not affiliated with MoneyControl.
